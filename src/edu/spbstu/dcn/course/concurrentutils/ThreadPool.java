package edu.spbstu.dcn.course.concurrentutils;

import java.util.LinkedList;

/**
 * Created by alexander on 03.10.14.
 */
public class ThreadPool {
    private final int maxThreadCount = 3;

    private final LinkedList<WorkedThread> threads;
    private final LinkedList<WorkedThread> freeThreads;
    private final Object lock;

    public ThreadPool() {
        this.threads = new LinkedList<WorkedThread>();
        this.freeThreads = new LinkedList<WorkedThread>();
        this.lock = new Object();
    }

    public void setTask(Runnable task) {
//        if(task == null) return;

        synchronized (lock) {

            if(freeThreads.isEmpty())
                makeNewThread();

            while (freeThreads.isEmpty()) {
                try {
                    lock.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

            freeThreads.remove().setCurrentTask(task);
        }
    }

    private void makeNewThread(){
        synchronized (lock) {
            if(threads.size() < maxThreadCount) {
                WorkedThread newWorkedThread = new WorkedThread("Aaa", this);//number

                new Thread(newWorkedThread).start();
                threads.push(newWorkedThread);
                freeThreads.push(newWorkedThread);

                lock.notify();
            }

        }
    }

    public void taskDone(Runnable workedThread){
        synchronized (lock) {
            freeThreads.push((WorkedThread) workedThread);
            lock.notify();
        }
    }

    public void close() {
        for (WorkedThread worker : threads) {
            worker.close();
        }
    }

}
