package edu.spbstu.dcn.course.concurrentutils;

import edu.spbstu.dcn.course.concurrentutils.ThreadPool;

/**
 * Created by alexander on 03.10.14.
 */
public class WorkedThread implements Closeable {

    private Runnable currentTask = null;
    private final Object lock;
    private final ThreadPool pool;
    private final String name;

    public WorkedThread(String name, ThreadPool pool) {
        lock = new Object();
        this.pool = pool;
        this.name = name;
    }

    public void setCurrentTask(Runnable task){


            if (currentTask == null) {
                synchronized (lock) {
                    currentTask = task;
                    lock.notify();
                }
            } else {
                try {
                    throw new IllegalAccessException("EX");
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
            }

    }

    @Override
    public void run() {
         while(true) {

            while (currentTask == null) {
                try {
                    lock.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

            synchronized (lock) {
                try {
                    currentTask.run();
                } finally {
                    currentTask = null;
                    pool.taskDone(this);
                }
            }

         }
    }

    public void close() {

    }

}
