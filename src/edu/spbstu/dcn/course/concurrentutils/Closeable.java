package edu.spbstu.dcn.course.concurrentutils;

/**
 * Created by alexander on 29.10.14.
 */

public interface Closeable extends Runnable {
    void close();
}
