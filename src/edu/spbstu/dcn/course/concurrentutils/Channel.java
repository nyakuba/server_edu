package edu.spbstu.dcn.course.concurrentutils;

import java.util.LinkedList;
import java.util.Objects;

/**
 * Created by alexander on 19.09.14.
 */
public class Channel implements Closeable {
    public Channel() {
        this.lock = new Object();
        this.queue = new LinkedList<Object>();
    }

    private final Object lock;
    private final int MAX_THREADS = 20;

    @Override
    public void run() {

    }

    private final LinkedList <Object> queue;

    public void put(Object obj) throws InterruptedException {
        synchronized (lock) {
            while(queue.size() >= MAX_THREADS){
                System.out.println("Wait put");
                lock.wait();
            }
            queue.push(obj);
            lock.notify();
        }
    }

    public Object get() throws InterruptedException {
//        lock.wait();
        synchronized (lock) {
            while(queue.isEmpty()) {
                System.out.println("Wait get");
                lock.wait();
            }
//            Object temp = queue.getLast();
//            queue.removeLast();

//            if(queue.getLast() == null)
//                Thread.ex

            lock.notify();
            return queue.removeLast();
        }
    }

    public boolean isEmpty() {
        synchronized (lock) {
            return queue.isEmpty();
        }
    }

    @Override
    public void close() {

    }
}
