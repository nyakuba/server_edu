package edu.spbstu.dcn.course.app;

import edu.spbstu.dcn.course.netutils.MessageHandler;

import java.io.*;
import java.util.Scanner;

/**
 * Created by alexander on 16.10.14.
 */

public class MessageHandlerImpl implements MessageHandler {
    private final int id;
    ProcessBuilder builder;
    Process process;
    OutputStream stdin;
    InputStream stdout;
    Scanner scan;
    BufferedReader reader;
    BufferedWriter writer;

    public MessageHandlerImpl(int number) throws IOException {
        id = number;
        builder = new ProcessBuilder("/bin/bash");
        builder.redirectErrorStream(true);
        process = builder.start();
        stdin = process.getOutputStream();
        stdout = process.getInputStream();
        scan = new Scanner(System.in);
        reader = new BufferedReader(new InputStreamReader(stdout));
        writer = new BufferedWriter(new OutputStreamWriter(stdin));
    }

//    public String handle(String message) {
//        System.out.println(id + " > " + message);
//        return message;
//    }

    public String handle(String message){

        String line = message;
        StringBuilder returnMessage = new StringBuilder();


        System.out.print("> ");

        ///while (scan.hasNext()) {
        if (message != null) {

            ///String input = scan.nextLine();
            String input = message;
            if (input.trim().equals("exit")) {
                try {
                    writer.write("exit\n");
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } else {
                //writer.write("((" + input + ") && echo --EOF--) || echo --EOF--\n");
                try {
                    writer.write(input + " && echo --EOF-- || echo --EOF--\n");
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            try {
                writer.flush();
            } catch (IOException e) {
                e.printStackTrace();
            }

            try {
                line = reader.readLine();
//                returnMessage.append(line);
            } catch (IOException e) {
                e.printStackTrace();
            }
            while (line != null && !line.trim().equals("--EOF--")) {
                System.out.println("Stdout: " + line);
                try {
                    returnMessage.append("\n").append(line);
                    line = reader.readLine();

                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            System.out.print("> ");
            returnMessage.append("\n").append("> ");
//            if (line == null) {
//                break;
//            }
        }

        return returnMessage.toString();
    }
}
