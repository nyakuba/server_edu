package edu.spbstu.dcn.course.app;

import edu.spbstu.dcn.course.netutils.MessageHandler;
import edu.spbstu.dcn.course.netutils.MessageHandlerFactory;

import java.io.IOException;

/**
 * Created by alexander on 16.10.14.
 */

public class MessageHandlerFactoryImpl implements MessageHandlerFactory {
    private static int counter = 0;

    @Override
    public MessageHandler create() throws IOException {
        return new MessageHandlerImpl(counter++);
    }
}
