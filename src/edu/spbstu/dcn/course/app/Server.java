package edu.spbstu.dcn.course.app;

import edu.spbstu.dcn.course.concurrentutils.Channel;
import edu.spbstu.dcn.course.concurrentutils.ThreadPool;
import edu.spbstu.dcn.course.netutils.Dispecher;
import edu.spbstu.dcn.course.netutils.Host;
import edu.spbstu.dcn.course.netutils.MessageHandlerFactory;

import java.io.IOException;


public class Server {
    public static int MAX_SESSIONS_COUNT = 5;
    public static void main(String[] args) {


        int port = 3357;
//        int port = Integer.parseInt(args[0]);

        Channel channel = new Channel();
        final ThreadPool threadPool = new ThreadPool();
        MessageHandlerFactory messageFactory = new MessageHandlerFactoryImpl();



        final Dispecher dispatcher = new Dispecher(channel, threadPool, messageFactory);

        final Host host = new Host(port, channel);


        Runtime.getRuntime().addShutdownHook(new Thread(new Runnable() {
            @Override
            public void run() {
                System.out.println("closing everything");
                host.close();
                dispatcher.close();
                threadPool.close();
            }
        }));

//        new Thread(new Dispecher(channel, threadPool, messageFactory), "Dispecher").start();

        dispatcher.start();
        host.start();

        //        new Thread(new Host(port, channel), "Host").start();
        //        int port = 3357;
//        if (args.length < 1) {
//            System.out.println("using port 3357");
//        } else {
//            port = Integer.parseInt(args[0]);
//        }
//        try {
//            ServerSocket serverSocket = new ServerSocket(port);
//            while (true) {
////                if (Thread.activeCount() <= MAX_SESSIONS_COUNT) {
//                    new Thread(new Session(serverSocket.accept())).start();
////                } else {
////                    Thread.sleep(1000);
////                }
//            }
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//        finally {
//            serverSocket.close();
//        }
    }


}
