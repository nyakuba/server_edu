package edu.spbstu.dcn.course.app;

import java.io.*;
import java.net.*;
import java.net.InetAddress;
import java.util.Scanner;


public class Client {
    public static void main(String[] args) {
        try {
            InetAddress address = InetAddress.getByName("localhost");
            int port = 3357;
//            if (args.length >= 2) {
//                System.out.println("using localhost:3357");
//                address = InetAddress.getByName(args[0]);
//                port = Integer.parseInt(args[1]);
//            }
            Socket socket = new Socket(address, port);
            System.out.println("opening socket");
           // String str = "Hi! Hi!";
            DataOutputStream dOS = new DataOutputStream(socket.getOutputStream());
            DataInputStream dIS = new DataInputStream(socket.getInputStream());

            Scanner scanner = new Scanner(System.in);
            while (true) {
                String str = scanner.nextLine();
                if (str.compareTo("quit") == 0) break;
                dOS.writeUTF(str);
                str = dIS.readUTF();
                System.out.println(str);
            }
        } catch (ConnectException e) {
            System.out.println("Cannot connect to host.");
        } catch (UnknownHostException e) {
            System.out.println("Unknown host.");
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
