package edu.spbstu.dcn.course.netutils;

import edu.spbstu.dcn.course.concurrentutils.Channel;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * Created by alexander on 19.09.14.
 */
public class Host implements Runnable {

    private static ServerSocket serverSocket;
    private boolean isWorking;
    private final int port;
    private final Channel channel;
    private final Thread thread;

    public Host(int p, Channel ch){
        port = p;
        channel = ch;

        try {
            serverSocket = new ServerSocket(port);
        } catch (IOException e) {
            e.printStackTrace();

            //TODO завершить работу //вынести в run?
        }

        isWorking = false;
        thread = new Thread(this, "Host with port " + p);
    }

    @Override
    public void run() {
        try {
            while (isWorking) {
                try {
                    channel.put(serverSocket.accept());
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            close();
        }

//        try {
//            ServerSocket serverSocket = new ServerSocket(port);
//
////            Channel channel = new Channel();
////            Dispecher dispecher = new Dispecher(channel);
//
//            Socket sct;
//
////            new Thread(dispecher).start();
//
//            while(true){
//                sct = serverSocket.accept();
//                channel.put(sct);
//            }
//
//        } catch (IOException e) {
//            e.printStackTrace();
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }
    }

    public void start(){
        isWorking = true;
        thread.start();
    }

    public void close() {
        if(isWorking) {
            System.out.println("closing serverSocket from Host " + serverSocket.getInetAddress().getCanonicalHostName());
            try {
                serverSocket.close();
            } catch (IOException e) {
                e.printStackTrace();
                // ignore
            }
        }
    }
}
