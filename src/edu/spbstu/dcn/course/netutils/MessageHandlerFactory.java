package edu.spbstu.dcn.course.netutils;

import java.io.IOException;

/**
 * Created by alexander on 16.10.14.
 */

public interface MessageHandlerFactory {
    MessageHandler create() throws IOException;
}
