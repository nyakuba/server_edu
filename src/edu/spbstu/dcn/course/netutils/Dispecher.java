package edu.spbstu.dcn.course.netutils;

import edu.spbstu.dcn.course.concurrentutils.Channel;
import edu.spbstu.dcn.course.concurrentutils.ThreadPool;

import java.io.IOException;

/**
 * Created by alexander on 19.09.14.
 */
public class Dispecher implements Runnable {

    Channel channel;
    private final MessageHandlerFactory messageFactory;
    private final ThreadPool threadPool;
    private final Thread thread;
    private boolean isWorking;

    public Dispecher(Channel channel, ThreadPool pool, MessageHandlerFactory factory) {
        this.channel = channel;
        threadPool = pool;
        messageFactory = factory;
        isWorking = false;
        thread = new Thread(this, "thread from Dispecher");
    }




    @Override
    public void run() {
        ThreadPool pool = new ThreadPool();
        channel.run();

        while(true){
//            if (!channel.isEmpty()){
                try {
//                    new Thread(new Session((java.net.Socket) channel.get())).start();
                    try {
                        pool.setTask(new Session((java.net.Socket) channel.get(), messageFactory.create()));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
//            } else {
//                try {
//                    System.out.println("All Quiet on the Western Front");
//                    Thread.sleep(5000);
//                } catch (InterruptedException e) {
//                    e.printStackTrace();
//                }
//            }
        }
    }


    public void start(){
        isWorking = true;
        thread.start();
    }

    public void close() {
        if(isWorking) {
            System.out.println("closing Dispecher");
            threadPool.close();
        }
    }
}
