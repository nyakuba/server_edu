package edu.spbstu.dcn.course.netutils;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.EOFException;
import java.io.IOException;
import java.net.Socket;
import java.util.logging.SocketHandler;


public class Session implements Runnable {
    private final Socket socket;
    private final MessageHandler messageHandler;
//    public static volatile int nowThread = 0;

    public Session(Socket s, MessageHandler handler) {
        socket = s;
        messageHandler = handler;
    }

    @Override
    public void run() {


//        if (nowThread <= 2) {
//            System.out.println("NEW THREAD");
//            nowThread++;
//        } else {
//            System.out.println("lot of threads");
//            return;
//        }

//        try {
//            System.out.println(socket.getInetAddress().getCanonicalHostName() + ":" + socket.getPort() + " connected");
//            DataInputStream dIS = new DataInputStream(socket.getInputStream());
//            while (true) {
//                System.out.println(socket.getInetAddress().getCanonicalHostName()
//                        + ":" + socket.getPort() + " > " + dIS.readUTF());
//            }
////            Thread.sleep(12000);
////            System.out.println("wake up!");
//
//        } catch (IOException e1) {
//            e1.printStackTrace();
//        }

//        nowThread--;

        try {
            System.out.println(socket.getInetAddress().getCanonicalHostName() + ":" + socket.getPort() + " connected");
//            System.out.println(socket.getLocalPort());
            String message;
            DataInputStream dIS = new DataInputStream(socket.getInputStream());
            DataOutputStream dOS = new DataOutputStream(socket.getOutputStream());
            while (true) {
                message = messageHandler.handle(dIS.readUTF());
                System.out.println("closing adfsada");
                dOS.writeUTF(message);
            }
        } catch (EOFException e) {
            // ignore
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            close();
        }
    }

    public void close() {
        System.out.println("closing " + socket.getInetAddress().getCanonicalHostName() + ":" + socket.getPort());
        try {
            socket.close();
        } catch (IOException e) {
            e.printStackTrace();
            // ignore
        }
      }


}