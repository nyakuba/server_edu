package edu.spbstu.dcn.course.netutils;

import java.io.IOException;

/**
 * Created by alexander on 16.10.14.
 */

public interface MessageHandler {
    public abstract String handle(String message);
}
