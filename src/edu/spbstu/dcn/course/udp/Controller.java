package edu.spbstu.dcn.course.udp;

/**
 * Created by sasha on 26.12.14.
 */
public interface Controller {
    Message get() throws InterruptedException;
    void put(byte[] message, int length);
    void close();
}
