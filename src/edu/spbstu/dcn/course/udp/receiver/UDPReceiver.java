package edu.spbstu.dcn.course.udp.receiver;

import edu.spbstu.dcn.course.udp.Parameters;
import edu.spbstu.dcn.course.udp.Receiver;
import edu.spbstu.dcn.course.udp.Sender;
import edu.spbstu.dcn.course.udp.sender.ControllerSender;
import edu.spbstu.dcn.course.udp.sender.FileReader;
import edu.spbstu.dcn.course.udp.sender.Timer;

import java.io.FileNotFoundException;
import java.net.SocketException;

/**
 * Created by sasha on 26.12.14.
 */
public class UDPReceiver {

    public static void main(String [] args){
        System.out.println("Hi! Im UDPReceiver");

        ControllerReceiver controllerReceiver = null;
        controllerReceiver = new ControllerReceiver();


        Receiver receiver = new Receiver (controllerReceiver, Integer.parseInt(args[1]));
        Sender sender = new Sender(controllerReceiver, Integer.parseInt(args[0]));

//        Runtime.getRuntime().addShutdownHook(new Thread(new Runnable() {
//            @Override
//            public void run() {
//                receiver.stop();
//                sender.stop();
//                controllerReceiver.close();
//            }
//        }, "Shutdown"));

        try {
            receiver.start();
            sender.start();
        } catch (SocketException e) {
            e.printStackTrace();
        }


    }

}
