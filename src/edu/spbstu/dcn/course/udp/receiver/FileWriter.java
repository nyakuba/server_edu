package edu.spbstu.dcn.course.udp.receiver;

import edu.spbstu.dcn.course.udp.Controller;
import edu.spbstu.dcn.course.udp.Message;
import edu.spbstu.dcn.course.udp.Parameters;
import sun.misc.IOUtils;

import java.io.*;
import java.util.Arrays;
import java.util.logging.Level;

/**
 * Created by sasha on 26.12.14.
 */
public class FileWriter implements Runnable{

    private ControllerReceiver controller;

    private final Thread thread;
    private boolean isRunning;
    File file;
    FileOutputStream outputStream;

    public FileWriter(ControllerReceiver controller) {
        this.controller = controller;
        this.isRunning = false;
        this.outputStream = null;
        this.file = null;
        this.thread = new Thread(this, "FileWriter");
    }

    public void start() throws FileNotFoundException {
        isRunning = true;
        thread.start();
    }

    public void stop() throws IOException {
        isRunning = false;
        outputStream.close();
        thread.interrupt();
    }

    @Override
    public void run() {

        final Message name;
        try {
            name = controller.getBlock();
            System.out.println("Filename " + new String(name.data, 0, name.length, "UTF-8") + ", length = " + name.length);
            file = new File(new String(name.data, 0, name.length, "UTF-8"));
            outputStream = new FileOutputStream(file);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        while (isRunning) {
            try {
                final Message block = controller.getBlock();
                System.out.println("FIleWriter get block with length  " + block.length);
                // TODO shutdown
//                if(block.length == 0){
//                    System.exit(0);
//                }
                if (block.length <= Parameters.BYTES_TO_ID) {
                    isRunning = false;
                    outputStream.close();
                } else {
                    outputStream.write(block.data, 0, block.length);
                }
            } catch (IOException e) {
                e.printStackTrace();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

}
