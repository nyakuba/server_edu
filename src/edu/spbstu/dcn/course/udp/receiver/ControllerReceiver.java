package edu.spbstu.dcn.course.udp.receiver;

import edu.spbstu.dcn.course.concurrentutils.Channel;
import edu.spbstu.dcn.course.udp.Controller;
import edu.spbstu.dcn.course.udp.Message;
import edu.spbstu.dcn.course.udp.Parameters;

import java.io.FileNotFoundException;
import java.io.IOException;

/**
 * Created by sasha on 26.12.14.
 */
public class ControllerReceiver implements Controller {

    private Object lock = new Object();
    private Channel channelWriter;
    private Channel channelSender;
    private int WantWriteIndex;
    private byte[] bufferIndex;
    private byte[] bufferMessage;
    private final Message [] saveToWrite;
    private final Message [] saveIndex;
    private FileWriter fileWriter;

    ControllerReceiver() {
        this.channelWriter = new Channel();
        this.channelSender = new Channel();
        this.WantWriteIndex = 0;
        this.bufferIndex = new byte[Parameters.BYTES_TO_ID];
        this.bufferMessage = new byte[Parameters.PACKET_SIZE];
        try {
            fileWriter = new FileWriter(this);
            fileWriter.start();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        saveToWrite = new Message[Parameters.WINDOW_SIZE];
        saveIndex = new Message[Parameters.WINDOW_SIZE];
        for (int i = 0; i < Parameters.WINDOW_SIZE; ++i) {
            saveToWrite[i] = new Message();
            saveToWrite[i].data = new byte[Parameters.PACKET_SIZE];
            saveToWrite[i].length = -1;
            saveIndex[i] = new Message();
            saveIndex[i].data = new byte[Parameters.BYTES_TO_ID];
            saveIndex[i].length = 4;
        }
    }


    /**
     * Выдаем Message, содержащий подтверждение
     * @return
     * @throws InterruptedException
     */
    @Override
    public Message get() throws InterruptedException {
        return (Message) channelSender.get();
    }

    /**
     * Кладем в контроллер очередную порцию данных
     * @param message
     */
    @Override
    public void put(byte[] message, int length) {
        synchronized (lock){
            int index = 0;
            // TODO вынести в отдельную функцию нафиг
            for(int i = 0; i < Parameters.BYTES_TO_ID; i++){
                index <<= 8;
                index += (int)bufferIndex[i];
            }

            System.out.println("WantWriteIndex = " + WantWriteIndex);
            System.out.println("index = " + index);

            saveToWrite[index % Parameters.WINDOW_SIZE].length = length - Parameters.BYTES_TO_ID;
            System.out.println("saveToWrite length " + saveToWrite[index % Parameters.WINDOW_SIZE].length);
            System.arraycopy(message, Parameters.BYTES_TO_ID, saveToWrite[index % Parameters.WINDOW_SIZE].data, 0, length);
            System.arraycopy(message, 0, saveIndex[index % Parameters.WINDOW_SIZE].data, 0, Parameters.BYTES_TO_ID);

            try {
                channelSender.put(saveIndex[index % Parameters.WINDOW_SIZE]);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            while(saveToWrite[WantWriteIndex % Parameters.WINDOW_SIZE].length != -1) {
                try {
                    System.out.println(saveToWrite[WantWriteIndex % Parameters.WINDOW_SIZE].length);
                    // и выделение памяти ушло
                    System.out.println("put block " + saveIndex[WantWriteIndex % Parameters.WINDOW_SIZE].data[3]
                            + " length " + saveToWrite[WantWriteIndex % Parameters.WINDOW_SIZE].length);
                    channelWriter.put(saveToWrite[WantWriteIndex % Parameters.WINDOW_SIZE]);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                WantWriteIndex++;
            }
        }
    }

    /**
     * Выдаем FileWriter'у очередную порцию данных для записи
     * @return
     * @throws InterruptedException
     */
    public Message getBlock() throws InterruptedException {
        return (Message) channelWriter.get();
    }

    @Override
    public void close() {
        try {
            fileWriter.stop();
            channelSender.close();
            channelWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
