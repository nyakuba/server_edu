package edu.spbstu.dcn.course.udp;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketException;

/**
 * Created by sasha on 26.12.14.
 */
public class Receiver implements Runnable {

    private final Controller controller;

    private final Thread thread;
    private boolean isRunning;
    private DatagramSocket socket;
    private DatagramPacket packet;
    private final int port;

    public Receiver(Controller controller, int port) {
        this.thread = new Thread(this, "Receiver");
        this.controller = controller;
        this.isRunning = false;
        this.port = port;
        this.packet = new DatagramPacket(new byte[Parameters.PACKET_SIZE + Parameters.BYTES_TO_ID], Parameters.PACKET_SIZE + Parameters.BYTES_TO_ID);
        this.socket = null;
    }

    public void start() throws SocketException {
        isRunning = true;
        this.socket = new DatagramSocket(port);
        thread.start();
    }

    void stop() {
        isRunning = false;
        socket.close();
        thread.interrupt();
    }

    @Override
    public void run() {
        System.out.println("Hi! Im Receiver!");
        while (isRunning) {
            try {
                socket.receive(packet);
                System.out.println("Receiver packet.getData.length " + packet.getLength());
                controller.put(packet.getData(), packet.getLength());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

}
