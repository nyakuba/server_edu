package edu.spbstu.dcn.course.udp;

import java.io.IOException;
import java.net.*;

/**
 * Created by sasha on 26.12.14.
 */
public class Sender implements Runnable {

    private final Controller controller;

    private final Thread thread;
    private boolean isRunning;
    private DatagramSocket socket;
    private DatagramPacket packet;
    private final int port;
//    private InetAddress address;

    public Sender(Controller controller, int port) {
        this.thread = new Thread(this, "Sender");
        this.controller = controller;
        this.isRunning = false;
        this.port = port;
        this.packet = new DatagramPacket(new byte[4], 4);
        this.socket = null;
    }

    public void start() throws SocketException {
        isRunning = true;
        this.socket = new DatagramSocket();
        thread.start();
    }

    void stop() {
        isRunning = false;
        socket.close();
        thread.interrupt();
    }

    @Override
    public void run() {
        System.out.println("Hi! Im Sender!");
        while (isRunning) {
            try {
                try {
                    Message data = controller.get();
                    //todo не пересоздавать пакет
                    packet = new DatagramPacket(data.data, data.length, InetAddress.getByName("localhost"), port);
                    System.out.println("SEND");
                    socket.send(packet);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
