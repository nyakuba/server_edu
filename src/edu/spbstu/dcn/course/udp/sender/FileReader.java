package edu.spbstu.dcn.course.udp.sender;

import edu.spbstu.dcn.course.udp.Controller;

import java.io.*;
import java.util.logging.Level;

/**
 * Created by sasha on 26.12.14.
 */
public class FileReader implements Runnable {

    private ControllerSender controller;

    private final Thread thread;
    private boolean isRunning;
    private final Object lock = new Object();
    private String filename;
    private File file;
    private InputStream inputStream;
    private byte[] buffer;
    private final int messageSize;

    public FileReader(ControllerSender controller, String filename, int messageSize) {
        this.isRunning = false;
        this.messageSize = messageSize;
        this.buffer = new byte[messageSize];
        this.filename = filename;
        this.controller = controller;
        this.thread = new Thread(this, "FileReader");
    }

    public void start() throws FileNotFoundException {
        isRunning = true;
        file = new File(filename);
        inputStream = new FileInputStream(file);
        thread.start();
    }

    public void stop() {
        try {
            isRunning = false;
            inputStream.close();
            thread.interrupt();
        } catch (IOException e) {
            // ignore
        }
    }

    @Override
    public void run() {
        try {
            final byte[] nameOfNewFile = (filename + "_UDP").getBytes("UTF-8");
            System.out.println("1st packet name of file: " + filename + "_UDP, byte length = " + nameOfNewFile.length);
            controller.putBlock(nameOfNewFile, nameOfNewFile.length);
            while (isRunning) {
                try {
                    int read = inputStream.read(buffer, 0, messageSize);
                    while (read != -1) {
                        controller.putBlock(buffer, read);
                        read = inputStream.read(buffer, 0, messageSize);
                    }
                    controller.putBlock(new byte[4], 4);
                    isRunning = false;
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
