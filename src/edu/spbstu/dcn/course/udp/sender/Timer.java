package edu.spbstu.dcn.course.udp.sender;

import edu.spbstu.dcn.course.udp.Controller;
import edu.spbstu.dcn.course.udp.Parameters;

import java.util.logging.Level;

/**
 * Created by sasha on 26.12.14.
 */
public class Timer implements Runnable {

    private ControllerSender controller;

    private final Thread thread;
    private final int [] counters;
    private long shutdownCounter;
    private boolean isRunning;
    private final Object lock = new Object();

    Timer(ControllerSender controller, int size){
        this.controller = controller;
        this.counters = new int[size];
        for (int i = 0; i < size; ++i)
            counters[i] = 0;
        this.isRunning = false;
        this.shutdownCounter = Parameters.LIMIT_OF_RESEND;
        this.thread = new Thread(this, "Timer");
    }



    public void start() {
        isRunning = true;
        thread.start();
    }

    public void stop() {
        isRunning = false;
        thread.interrupt();
    }

    public void startTimer(int index) {
        synchronized (lock) {
            System.out.println("startTimer for " + index);
            counters[index] = Parameters.NUMBER_ITERATIONS;
        }
    }

    public void stopTimer(int index) {
        synchronized (lock) {
            counters[index] = 0;
        }
    }


    @Override
    public void run() {
        while (isRunning) {

            synchronized (lock) {
                --shutdownCounter;//TODO
                if (shutdownCounter == 0) {
                    System.out.println("Timer shutdown!");
                    System.exit(0);
                }
                for (int i = 0; i < counters.length; ++i){
                    if (counters[i] == 0)
                        continue;
                    if (--counters[i] == 0) {
                        controller.resend(i);
                    }
                }
            }

            try {
                thread.sleep(Parameters.ITERATION_TIME);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }


        }
    }
}
