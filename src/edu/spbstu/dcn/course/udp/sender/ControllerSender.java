package edu.spbstu.dcn.course.udp.sender;

import edu.spbstu.dcn.course.concurrentutils.Channel;
import edu.spbstu.dcn.course.udp.Controller;
import edu.spbstu.dcn.course.udp.Message;
import edu.spbstu.dcn.course.udp.Parameters;

import java.nio.ByteBuffer;

/**
 * Created by sasha on 26.12.14.
 */
public class ControllerSender implements Controller {

    private Object lock = new Object();
    private int maxIndex;
    private Timer timer;
    private final Message[] window;
    private Channel channel;
    private int leftIndex;
    private boolean isSend[];

    ControllerSender(){
        this.window = new Message[Parameters.WINDOW_SIZE];
        this.timer = new Timer(this, Parameters.WINDOW_SIZE);
        this.isSend = new boolean[Parameters.WINDOW_SIZE];
        this.timer.start();
        for (int i = 0; i < Parameters.WINDOW_SIZE; ++i) {
            window[i] = new Message();
            window[i].data = new byte[Parameters.BYTES_TO_ID + Parameters.PACKET_SIZE];
            isSend[i] = false;
        }
        channel = new Channel();
        leftIndex = 0;
        maxIndex = 0;
    }

    /**
     * Кладем подтверждение от ресивера.
     * @param message
     */
    @Override
    public void put(byte[] message, int length) {
        synchronized (lock) {
            int globalIndexOK = 0;
            for(int i = 0; i < Parameters.BYTES_TO_ID; i++){
                globalIndexOK <<= 8;
                globalIndexOK += (int)message[i];
            }
            System.out.println("globalIndexOK = " + globalIndexOK);
            int localIndexOK = globalIndexOK % Parameters.WINDOW_SIZE;
            timer.stopTimer(localIndexOK);
            isSend[localIndexOK] = true;
            while(isSend[leftIndex % Parameters.WINDOW_SIZE]) {
                isSend[leftIndex % Parameters.WINDOW_SIZE] = false;
                leftIndex++;
            }
        }
    }

    /**
     * Забираем очередное сообщение на отправку
     * @return
     * @throws InterruptedException
     */
    @Override
    public Message get() throws InterruptedException {
        Message data = (Message) channel.get();
        System.out.println("taking new data with length = " + data.length);
        return data;
    }

    /**
     * Кладем считанное сообщение в контроллер
     * @param message
     * @throws InterruptedException
     */
    public void putBlock(byte[] message, int length) throws InterruptedException {
        synchronized (lock){
            while(maxIndex + 1 > leftIndex + Parameters.WINDOW_SIZE){
                lock.wait();
            }//todo ??? maxIndex + 1
            byte[] index = ByteBuffer.allocate(Parameters.BYTES_TO_ID).putInt(maxIndex).array();
            System.arraycopy(message, 0, window[maxIndex % Parameters.WINDOW_SIZE].data, Parameters.BYTES_TO_ID, length);
            System.arraycopy(index, 0, window[maxIndex % Parameters.WINDOW_SIZE].data, 0, Parameters.BYTES_TO_ID);
            window[(maxIndex) % Parameters.WINDOW_SIZE].length = length;
            channel.put(window[(maxIndex) % Parameters.WINDOW_SIZE]);
            timer.startTimer((maxIndex) % Parameters.WINDOW_SIZE);
            maxIndex = (maxIndex + 1);
            System.out.println("maxIndex = " + maxIndex);
            lock.notify();
        }
    }

    public void resend(int id){//TODO синхронизация?
        try {
            channel.put(window[id]);
            timer.startTimer(id);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void close() {
        channel.close();
        timer.stop();
    }


}
