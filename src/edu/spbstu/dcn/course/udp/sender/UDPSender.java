package edu.spbstu.dcn.course.udp.sender;

import edu.spbstu.dcn.course.udp.Parameters;
import edu.spbstu.dcn.course.udp.Receiver;
import edu.spbstu.dcn.course.udp.Sender;

import java.io.FileNotFoundException;
import java.net.SocketException;

/**
 * Created by sasha on 26.12.14.
 */
public class UDPSender {

    public static void main(String [] args){
        System.out.println("Hi! Im UDPSender");

        ControllerSender controllerSender = new ControllerSender();

        FileReader fileReader = new FileReader(controllerSender, args[2], Parameters.PACKET_SIZE);
        Receiver receiver = new Receiver (controllerSender, Integer.parseInt(args[0]));
        Sender sender = new Sender(controllerSender, Integer.parseInt(args[1]));

//        Runtime.getRuntime().addShutdownHook(new Thread(new Runnable() {
//            @Override
//            public void run() {
//                receiver.stop();
//                fileReader.stop();
//                sender.stop();
//                controller.close();
//            }
//        }, "Shutdown"));

        try {
            fileReader.start();
            receiver.start();
            sender.start();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (SocketException e) {
            e.printStackTrace();
        }


    }

}
