package edu.spbstu.dcn.course.udp;

/**
 * Created by sasha on 26.12.14.
 */
public class Parameters {
    public final static int BYTES_TO_ID = Integer.BYTES;
    public final static int PACKET_SIZE = 512;

    public final static int ITERATION_TIME = 50;
    public final static int NUMBER_ITERATIONS = 10;

    public final static int LIMIT_OF_RESEND = 100;

    public final static int WINDOW_SIZE = 5;
}
